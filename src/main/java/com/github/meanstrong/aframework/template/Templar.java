package com.github.meanstrong.aframework.template;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.meanstrong.aframework.expression.Expression;
import com.github.meanstrong.aframework.vars.VariableManager;
import com.hubspot.jinjava.Jinjava;

public class Templar {
	private Map<String, Object> _available_variables;
	
	public Templar(Map<String, Object> variables){
		this._available_variables = variables;
	}
	
	public Object template(Object variable){
		Object result = null;
		if(variable instanceof String){
			result = variable;
			if (this._contains_vars(variable)){
				String variable_string = (String) variable;
				if (variable_string.startsWith("{{") && variable_string.endsWith("}}") && !variable_string.contains("|")
						&& variable_string.lastIndexOf("{{") == 0){
					variable_string = variable_string.substring(2, variable_string.length()-2);
					VariableManager vm = new VariableManager(this._available_variables);
					Expression expr = new Expression(vm);
					result = expr.eval_value(variable_string);
				} else {
					Jinjava jinjava = new Jinjava();
					jinjava.getGlobalContext().registerFilter(new TojsonFilter());
					result = jinjava.render(variable_string, this._available_variables);
				}
			}
		}else if(variable instanceof List){
			result = new ArrayList<Object>();
			for (Object entry : (List<Object>) variable) {
				((List) result).add(this.template(entry));
			}
		}else if(variable instanceof Map){
			result = new HashMap<String, Object>();
			for (Map.Entry<String, Object> entry : ((Map<String, Object>) variable).entrySet()) {
				((Map<String, Object>) result).put(entry.getKey(), this.template(entry.getValue()));
			}
		}else{
			result = variable;
		}
		return result;
	}
	
	public void set_available_variables(Map<String, Object> variables){
		this._available_variables = variables;
	}
	private boolean _contains_vars(Object data){
		if (data instanceof String){
			String data_string = (String) data;
			if (data_string.contains("{{") || data_string.contains("{%")){
				return true;
			}
		}
		return false;
	}
}