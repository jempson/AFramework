package com.github.meanstrong.aframework.executor;

import com.github.meanstrong.aframework.parsing.DataLoader;
import com.github.meanstrong.aframework.plugins.ActionLoader;
import com.github.meanstrong.aframework.result.Result;
import com.github.meanstrong.aframework.template.Templar;
import com.github.meanstrong.aframework.vars.VariableManager;

public class Playbook {
	private String playbook;
	// private Map<String, Object> task_vars;
	private VariableManager variable_manager;
	private Result result;
	private DataLoader _loader;

	public Playbook(String playbook, VariableManager variable_manager, Result result, DataLoader loader) {
		this.variable_manager = variable_manager;
		this.playbook = playbook;
		this.result = result;
		this._loader = loader;
	}

	// public Playbook(String playbook, Result result) {
	// this(playbook, new VariableManager(), result);
	// }
	//
	public Playbook(String playbook) {
		this(playbook, new VariableManager(), null, null);
	}

	public void run() {
		if (this.result == null) {
			this.result = new Result();
		}
		com.github.meanstrong.aframework.playbook.Playbook playbook = com.github.meanstrong.aframework.playbook.Playbook
				.load(this.playbook, this.variable_manager, this._loader);
		this._loader = playbook.get_loader();
		ActionLoader action_loader = new ActionLoader();
		for (com.github.meanstrong.aframework.playbook.Task task : playbook.get_entries()) {
			try {
				Templar templar = new Templar(this.variable_manager.all_vars());
				task.validate(templar);

				String action = (String) task.get_attr("action");
				if (action.equals("include")) {
					Include executor = new Include(task, this.variable_manager.all_vars(), this.result, this._loader);
					executor.run();
				} else if (action_loader.get(action, task) != null) {
					Task executor = new Task(task, this.variable_manager.all_vars(), this.result, this._loader);
					executor.run();
				} else {
					throw new RuntimeException("Unknow task action: " + action);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Result get_result() {
		return this.result;
	}
}
