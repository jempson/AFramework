package com.github.meanstrong.aframework.executor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.github.meanstrong.aframework.parsing.DataLoader;
import com.github.meanstrong.aframework.result.Result;
import com.github.meanstrong.aframework.template.Templar;
import com.github.meanstrong.aframework.vars.VariableManager;

public class Include  extends Task {
	
	public Include(com.github.meanstrong.aframework.playbook.Task task, Map<String, Object> job_vars, Result result, DataLoader loader) {
		super(task, job_vars, result, loader);
	}
	
	@Override
	public Map<String, Object> _execute() {
		this.task_vars.putAll(this.task.get_variable_manager().all_vars());
		Templar templar = new Templar(this.task_vars);
		this.task.post_validate(templar);
		int retries = this.task.retries;
		double delay = this.task.delay;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("failed", false);
		if (!this.task.when.isEmpty() && !this.task.eval_condition(this.task.when, this.task_vars)) {
			result.put("failed", false);
			result.put("skipped", true);
			return result;
		}
//		String action = this.task.action.substring(0, 1).toUpperCase() + this.task.action.substring(1);
		try {
			for(int i = 0; i<retries; i++){
				Map<String, Object> args = (Map<String, Object>) templar.template(this.task.args);
				String filename = (String) args.get("name");
				Map<String, Object> vars = (Map<String, Object>) args.get("vars");
				VariableManager variable_manager = new VariableManager();
				variable_manager.set_playbook_vars(this.task.get_variable_manager().all_vars());
				variable_manager.set_playbook_vars(vars);
				Playbook executor = new Playbook(filename, variable_manager, this.result, this._loader);
				executor.run();
//				task_vars.put("result", result);
				if (!this.task.util.isEmpty() && this.task.eval_condition(this.task.util, this.task_vars)) {
					break;
				}
				Thread.sleep((int) delay*1000);
			}
		} catch (Exception e) {
			result.put("failed", true);
			result.put("msg", "Unexpected failure during action handler execution.");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			result.put("exception", errors.toString());
		}
		return result;
	}
}
