package com.github.meanstrong.aframework.expression;

import com.github.meanstrong.aframework.expression.function.Function;
import com.github.meanstrong.aframework.expression.operator.Operator;

public class ExpressionToken {
	public enum TokenType{
		TOKEN_TYPE_CONSTANT,
		TOKEN_TYPE_OPERATOR,
		TOKEN_TYPE_FUNCTION,
	}
	private TokenType token_type ;
	private Constant constant;
	private Operator operator ;
	private Function function;
	ExpressionToken(TokenType token_type, Constant constant){
		this.token_type = token_type;
		this.constant = constant;
	}
	ExpressionToken(TokenType token_type, Operator operator){
		this.token_type = token_type;
		this.operator = operator;
	}
	ExpressionToken(TokenType token_type, Function function){
		this.token_type = token_type;
		this.function = function;
	}
	public TokenType get_token_type(){
		return this.token_type;
	}
	public Constant get_token_value_constant(){
		return this.constant;
	}
	public Operator get_token_value_operator(){
		return this.operator;
	}
	public Function get_token_value_function(){
		return this.function;
	}
}
