package com.github.meanstrong.aframework.expression.operator;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class OperatorOR extends Operator {
	OperatorOR(){
		super("||", 13, 2);
	}
	public Constant evaluate(List<Constant> args){
		super.check_args_num(args.size());
		if(args.get(0).get_value_boolean()){
			return args.get(0);
		}
		return args.get(1);
	}
}
