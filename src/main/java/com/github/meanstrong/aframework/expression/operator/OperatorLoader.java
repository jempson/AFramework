package com.github.meanstrong.aframework.expression.operator;

import java.util.ArrayList;
import java.util.List;

public class OperatorLoader {
	public static List<Operator> all(){
		List<Operator> list = new ArrayList<Operator>();
		list.add(new OperatorAND());
		list.add(new OperatorEQ());
		list.add(new OperatorNEQ());
		list.add(new OperatorNOT());
		list.add(new OperatorOR());
		list.add(new OperatorDOT());
		list.add(new OperatorPLUS());
		return list;
	}
	public static Operator get(String name){
		for(Operator op: OperatorLoader.all()){
			if(op.get_op_string().equals(name)){
				return op;
			}
		}
		return null;
	}
}
