package com.github.meanstrong.aframework.expression.function;

import java.util.ArrayList;
import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class Extend extends Function{
	public Extend(){
		super("extend", 2);
	}
	@Override
	public Constant call(List<Constant> args){
		super.check_args_num(args.size());
		if(args.get(0).get_value() instanceof List && args.get(1).get_value() instanceof List ){
			List arg0 = (List) args.get(0).get_value();
			List arg1 = (List) args.get(1).get_value();
			List result = new ArrayList(){};
			result.addAll(arg0);
			result.addAll(arg1);
			return new Constant(result);
		}
		throw new RuntimeException("Function startswith args MUST all string.");
	}
}
