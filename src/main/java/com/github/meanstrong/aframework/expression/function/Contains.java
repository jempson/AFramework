package com.github.meanstrong.aframework.expression.function;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class Contains extends Function {
	public Contains(){
		super("contains", 2);
	}
	@Override
	public Constant call(List<Constant> args){
		super.check_args_num(args.size());
		Object arg0 = args.get(0).get_value();
		Object arg1 = args.get(1).get_value();
		if(arg0 instanceof String && arg1 instanceof String ){
			return new Constant(((String) arg0).contains((String) arg1));
		}
		throw new RuntimeException("Function contains args MUST all string.");
	}
}