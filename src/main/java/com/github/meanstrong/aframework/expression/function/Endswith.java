package com.github.meanstrong.aframework.expression.function;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class Endswith extends Function {
	public Endswith(){
		super("endswith", 2);
	}
	@Override
	public Constant call(List<Constant> args){
		super.check_args_num(args.size());
		if(args.get(0).get_value() instanceof String && args.get(1).get_value() instanceof String ){
			return new Constant(((String) args.get(0).get_value()).endsWith((String) args.get(1).get_value()));
		}
		throw new RuntimeException("Function endswith args MUST all string.");
	}
}