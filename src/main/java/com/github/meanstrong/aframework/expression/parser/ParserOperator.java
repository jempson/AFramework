package com.github.meanstrong.aframework.expression.parser;

public class ParserOperator extends Parser{
	public Result parse(String target){
		String punctuation = "!#%&*+,-./:;<=>?@\\^_`{|}~";
		if(!punctuation.contains(String.valueOf(target.charAt(0)))){
			return Result.fail();
		}
		int step = 1;
		while(step < target.length() && punctuation.contains(String.valueOf(target.charAt(step)))){
			step += 1;
		}
		return Result.succeed(target.substring(0, step), target.substring(step));
	}
}
