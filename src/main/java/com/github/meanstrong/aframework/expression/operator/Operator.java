package com.github.meanstrong.aframework.expression.operator;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class Operator {
	private String op_string;
	private int priority;
	private int num_args;
	Operator(String op_string, int priority, int num_args){
		this.op_string = op_string;
		this.priority = priority;
		this.num_args = num_args;
	}
	public String get_op_string(){
		return this.op_string;
	}
	public int get_priority(){
		return this.priority;
	}
	public int get_num_args(){
		return this.num_args;
	}
	public Constant evaluate(List<Constant> args){
		return null;
	}
	public void check_args_num(int size){
		if(size != this.num_args){
			throw new RuntimeException("Operator args NOT equal.");
		}
	}
	public String toString(){
		return String.format("<Operator %s>", this.op_string);
	}
}
