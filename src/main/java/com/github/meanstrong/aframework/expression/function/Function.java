package com.github.meanstrong.aframework.expression.function;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public abstract class Function {
	private String name;
	private int num_args;
	public Function(String name, int num_args){
		this.name = name;
		this.num_args = num_args;
	}
	public String get_name(){
		return this.name;
	}
	public int get_num_args(){
		return this.num_args;
	}
	public void check_args_num(int size){
		if(size != this.num_args){
			throw new RuntimeException("Function args NOT equal.");
		}
	}
	public abstract Constant call(List<Constant> args);
	
	public String toString(){
		return String.format("<Function %s>", this.name);
	}
}
