package com.github.meanstrong.aframework.expression.function;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class Str extends Function{
	public Str(){
		super("str", 1);
	}
	@Override
	public Constant call(List<Constant> args){
		super.check_args_num(args.size());
		Object arg0 = args.get(0).get_value();
		if(arg0 instanceof String){
			return new Constant((String) arg0);
		}else if(arg0 instanceof Integer){
			return new Constant(String.valueOf((int) arg0));
		}
		throw new RuntimeException("Unknow function args type: "+arg0.getClass().getName());
	}
}
