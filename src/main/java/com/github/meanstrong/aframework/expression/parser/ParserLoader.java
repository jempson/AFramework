package com.github.meanstrong.aframework.expression.parser;

import java.util.ArrayList;
import java.util.List;

public class ParserLoader {
	public static List<Parser> all(){
		List<Parser> list = new ArrayList<Parser>();
		list.add(new ParserFunction());
		list.add(new ParserBracket());
		list.add(new ParserString());
		list.add(new ParserNumber());
		list.add(new ParserVariable());
		list.add(new ParserOperator());
		return list;
	}
}
