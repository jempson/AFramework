package com.github.meanstrong.aframework.expression.parser;

public class ParserString extends Parser{
	public Result parse(String target) {
		if(target.charAt(0) != '"' && target.charAt(0) != '\''){
			return Result.fail();
		}
		char s = target.charAt(0);
		int step = 1;
		while(step < target.length() && target.charAt(step) != s){
			step += 1;
		}
		if(target.charAt(step) == s){
			return Result.succeed(target.substring(0, step+1), target.substring(step+1));
		}
		return Result.fail();
	}
}
