package com.github.meanstrong.aframework.expression.operator;

import java.util.List;

import com.github.meanstrong.aframework.expression.Constant;

public class OperatorNOT extends Operator {
	OperatorNOT(){
		super("!", 2, 1);
	}
	public Constant evaluate(List<Constant> args){
		super.check_args_num(args.size());
		boolean result = !args.get(0).get_value_boolean();
		return new Constant(result);
	}
}
