package com.github.meanstrong.aframework.expression.function;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.github.meanstrong.aframework.expression.Constant;

public class Tojson extends Function {
	public Tojson(){
		super("to_json", 1);
	}
	@Override
	public Constant call(List<Constant> args){
		super.check_args_num(args.size());
		Object arg0 = args.get(0).get_value();
		if(arg0 instanceof String){
			return new Constant(JSONObject.parseObject((String) arg0));
		}
		throw new RuntimeException("Function to_json args MUST all string.");
	}
}