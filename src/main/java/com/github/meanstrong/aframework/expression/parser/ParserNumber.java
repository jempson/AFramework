package com.github.meanstrong.aframework.expression.parser;

public class ParserNumber extends Parser{

	public Result parse(String target) {
		if(!Character.isDigit(target.charAt(0))){
			return Result.fail();
		}
		int step = 1;
		while(step < target.length() && Character.isDigit(target.charAt(step))){
			step += 1;
		}
		return Result.succeed(target.substring(0, step), target.substring(step));
	}
}
