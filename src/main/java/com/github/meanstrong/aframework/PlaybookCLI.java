package com.github.meanstrong.aframework;

import java.io.FileNotFoundException;

import com.github.meanstrong.aframework.plugins.Loader;
import com.github.meanstrong.aframework.executor.Playbook;
import com.github.meanstrong.aframework.result.Result;



public class PlaybookCLI {

	public static void main(String[] args) throws FileNotFoundException  {
    	Loader loader = new Loader("");
    	loader.load_jar("requires");

		String filename = "testcase.yml";
		for (int i = 0; i < args.length; i++){
			if (args[i].startsWith("--testcase-file=")){
				filename = args[i].split("=")[1];
			}
		}
//		Result result = new Result();
		Playbook executor = new Playbook(filename);
		executor.run();
		System.out.println(executor.get_result());
	}
}
