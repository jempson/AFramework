package com.github.meanstrong.aframework.playbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import com.github.meanstrong.aframework.parsing.DataLoader;
import com.github.meanstrong.aframework.vars.VariableManager;

public class Playbook {
	private List<Task> entries;
	private VariableManager variable_manager;
	private DataLoader _loader;

	public Playbook(DataLoader loader) {
		this._loader = loader;
	}

	public static Playbook load(String filename, VariableManager variable_manager) {
		return Playbook.load(filename, variable_manager, null);
	}

	public static Playbook load(String filename, VariableManager variable_manager, DataLoader loader) {
		Playbook playbook = new Playbook(loader);
		return playbook.load_from_file(filename, variable_manager);
	}

	public Playbook load_from_file(String filename, VariableManager variable_manager) {
		if (this._loader == null) {
			this._loader = new DataLoader();
			Path basedir = this._loader.get_base_dir().resolve(filename).toAbsolutePath().getParent();
			this._loader.set_base_dir(basedir);
		}
		this.entries = new ArrayList<Task>();
		for (Map<String, Object> entry : this._loader.load_from_file(new File(filename).getName())) {
			Task task = Task.load(entry, variable_manager, this._loader);
			this.entries.add(task);
		}
		// String basedir = System.getProperty("user.dir");
		// filename = basedir + File.separator + "resources" + File.separator +
		// filename;
		// Yaml yaml = new Yaml();
		// File f = new File(filename);
		// this.entries = new ArrayList<Task>();
		// this.variable_manager = variable_manager;
		// try {
		// List<Map<String, Object>> result = (List<Map<String, Object>>)
		// yaml.load(new FileInputStream(f));
		// for (Map<String, Object> entry : result) {
		// Task task = Task.load(entry, variable_manager);
		// this.entries.add(task);
		// }
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// throw new RuntimeException(e);
		// }
		return this;
	}

	public List<Task> get_entries() {
		return this.entries;
	}

	public DataLoader get_loader() {
		return this._loader;
	}
}
