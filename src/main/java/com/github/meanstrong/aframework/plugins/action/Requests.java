package com.github.meanstrong.aframework.plugins.action;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;
import com.github.meanstrong.aframework.playbook.Task;

import net.dongliu.requests.Cookie;
import net.dongliu.requests.RawResponse;
import net.dongliu.requests.RequestBuilder;

public class Requests extends ActionBase {

	public Requests(Task task) {
		super(task);
	}

	public String get_string_vars(String name, Map<String, ?> vars) {
		return this.get_string_vars(name, vars, "");
	}

	public String get_string_vars(String name, Map<String, ?> vars, String def) {
		if (vars.get(name) == null) {
			return def;
		}
		return (String) vars.get(name);
	}

	@Override
	public Map<String, Object> run(Map<String, Object> task_vars) {
		// System.setProperty("https.protocols", "TLSv1.2");
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> args = (Map<String, Object>) this.task.args;
		String method = "GET";
		Object args_method = args.get("method");
		if (args_method != null) {
			method = (String) args_method;
		}
		String url = "";
		Object args_url = args.get("url");
		if (args_url != null) {
			url = (String) args_url;
		}
		RequestBuilder req = net.dongliu.requests.Requests.newRequest(method, url);
		Object args_auth = args.get("basic_auth");
		if (args_auth != null) {
			req = req.basicAuth(((List<String>) args_auth).get(0), ((List<String>) args_auth).get(1));
		}
		Object args_params = args.get("params");
		if (args_params != null) {
			req = req.params((Map<String, Object>) args_params);
		}
		Object args_data = args.get("data");
		if (args_data != null) {
			if (args_data instanceof String) {
				req = req.body((String) args_data);
			}
		}
		Object args_headers = args.get("headers");
		if (args_headers != null) {
			if (args_headers instanceof Map) {
				req = req.headers((Map<String, Object>) args_headers);
			}
		}
		Object args_formdata = args.get("formdata");
		if (args_formdata != null) {
			if (args_formdata instanceof String) {
				req = req.body((String) args_formdata);
			} else if (args_formdata instanceof Map) {
				req = req.forms((Map<String, Object>) args_formdata);
			}
		}
		Object args_jsondata = args.get("jsondata");
		if (args_jsondata != null) {
			if (args_jsondata instanceof Map) {
				req = req.body(new JSONObject((Map) args_jsondata).toString());
			}
		}
		Object args_cookies = args.get("cookies");
		if (args_cookies != null) {
			if (args_cookies instanceof Collection<?>) {
				req = req.cookies((Collection<Cookie>) args_cookies);
			} else if (args_cookies instanceof Map) {
				req = req.cookies((Map<String, Object>) args_cookies);
			}
		}
		req = req.verify(false);
		RawResponse resp = req.send();
		result.put("status_code", resp.getStatusCode());
		// result.put("headers", resp.getHeaders());
		result.put("cookies", resp.getCookies());
		String text = resp.readToText();
		if (resp.getFirstHeader("Content-Type").contains("application/json")) {
			result.put("json", JSONObject.parse(text));
		} else {
			result.put("text", text);
		}

		return result;
	}
}
