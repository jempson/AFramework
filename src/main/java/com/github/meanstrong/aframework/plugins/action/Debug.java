package com.github.meanstrong.aframework.plugins.action;

import java.util.HashMap;
import java.util.Map;

import com.github.meanstrong.aframework.playbook.Task;

public class Debug extends ActionBase {

	public Debug(Task task) {
		super(task);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object run(Map<String, Object> task_vars) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> args = (Map<String, Object>) this.task.args;
		if (args.get("var") != null) {
			result.put((String) args.get("var"), task_vars.get((String) args.get("var")));
		} else if (args.get("msg") != null) {
			result.put("msg", task_vars.get((String) args.get("msg")));
		} else {
			result.put("debug", args);
		}
		return result;
	}

}
