package com.github.meanstrong.aframework.plugins;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import com.github.meanstrong.aframework.plugins.action.ActionBase;


public class ActionLoader {
	private String pkg;
	
	public ActionLoader(){
		this.pkg = this.getClass().getPackage().getName()+".action";
	}
	
	public ActionBase get(String name, com.github.meanstrong.aframework.playbook.Task task){
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		ActionBase handler = null;
		try {
			Class<?> clazz = Class.forName(this.pkg+"."+name);
			Constructor<?> constructor = clazz.getConstructor(com.github.meanstrong.aframework.playbook.Task.class);
			handler = (ActionBase) constructor.newInstance(task);
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		return handler;
	}

	public boolean contains(String name){
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		Class<?> clazz;
		try {
			clazz = Class.forName(this.pkg+"."+name);
			Constructor<?> constructor = clazz.getConstructor(com.github.meanstrong.aframework.playbook.Task.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
		return true;
	}
}
