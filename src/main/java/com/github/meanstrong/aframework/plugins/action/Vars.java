package com.github.meanstrong.aframework.plugins.action;

import java.util.HashMap;
import java.util.Map;

import com.github.meanstrong.aframework.playbook.Task;

public class Vars extends ActionBase {

	public Vars(Task task) {
		super(task);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object run(Map<String, Object> task_vars) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> args = (Map<String, Object>) this.task.args;
		this.task.get_variable_manager().set_playbook_vars(args);
		result.put("set_vars", args);
		return result;
	}
}
