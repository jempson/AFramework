package com.github.meanstrong.aframework.plugins.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.meanstrong.aframework.playbook.Task;
import com.google.common.base.Joiner;

public class Shell extends ActionBase {

	public Shell(Task task) {
		super(task);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object run(Map<String, Object> task_vars) {
		Map<String, Object> result = new HashMap<String, Object>();
		String[] cmd = { "sh", "-c", (String) this.task.args };
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") >= 0) {
			cmd[0] = "cmd";
			cmd[1] = "/c";
		}
		result.put("cmd", (String) this.task.args);
		List<String> stdout_lines = new ArrayList<String>();
		List<String> stderr_lines = new ArrayList<String>();
		try {
			Process p = Runtime.getRuntime().exec(cmd);
			String line = null;
			BufferedReader stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = stdout.readLine()) != null && !line.isEmpty()) {
				stdout_lines.add(line);
			}
			result.put("stdout", Joiner.on(System.getProperty("line.separator")).join(stdout_lines));
			BufferedReader stderr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			line = null;
			while ((line = stderr.readLine()) != null) {
				stderr_lines.add(line);
			}
			result.put("stderr", Joiner.on(System.getProperty("line.separator")).join(stderr_lines));
			int rc = p.waitFor();
			result.put("rc", rc);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return result;
	}
}
