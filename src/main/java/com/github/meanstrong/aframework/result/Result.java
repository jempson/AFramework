package com.github.meanstrong.aframework.result;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.github.meanstrong.aframework.playbook.Task;

public class Result {
//	public ArrayList<Map> results = new ArrayList<Map>();
//	public void add_result(Map<?, ?> result) {
//		this.results.add(result);
//	}
	private List<Task> skipped;
	private List<Task> failures;
	private List<Task> errors;
	private int tests_run;
	public Result(){
		this.skipped = new ArrayList<Task>();
		this.failures = new ArrayList<Task>();
		this.errors = new ArrayList<Task>();
		this.tests_run = 0;
	}
	
	public void start_test(Task testcase){
		this.tests_run += 1;
		System.out.println("Task <"+testcase+"> start.");
	}
	public void stop_test(Task testcase){
		System.out.println("Task <"+testcase+"> stop.");
	}
	public void add_skipped(Task testcase){
		this.skipped.add(testcase);
		System.out.println("Task <"+testcase+"> run skipped.");
	}
	public void add_success(Task testcase){
		System.out.println("Task <"+testcase+"> run success.");
	}
	public void add_failure(Task testcase, String desc){
		this.failures.add(testcase);
		System.out.println("Task <"+testcase+"> run failure: "+desc);
	}
	public void add_error(Task testcase, String err){
		this.errors.add(testcase);
		System.out.println("Task <"+testcase+"> run error: "+err);
	}
	public void finalize(){
		String string = String.format("<%s run=%i errors=%i failures=%i>", this.tests_run, this.errors.size(), this.failures.size());
		System.out.println(string);
	}
	@Override
	public String toString(){
		return String.format("<Result run=%d errors=%d failures=%d>", this.tests_run, this.errors.size(), this.failures.size());
	}
}
