# AFramework - An automated testing framework for API test.

## About
AFramework是一套自动化测试框架。  
AFramework通过编写一份基于YAML语言的testcase测试。基于YAML使得testcase易于理解、更加简单和优雅。  
AFramework非常容易扩展，提供plugin式的扩展方式，便于开发更多功能。   

## Requirements
- Java (本人仅在java 1.7版本下开发完成，发布的release版本也都是在1.7版本下打包完成)

## Usage
```shell
java -jar AFramework.jar [--testcase-file=testcase.yml]
```
使用--help查看更多使用帮助。  

### Testcase
This is an example testcase-file: [testcase.yml](docs/github.yml)   
目前框架内支持的Action列表：
- debug(debug用，用于打印一个变量的值或者一个msg)
- defaults (定义变量值，若已存在则不改变)
- requests (HTTP request调用)
- shell (shell调用)
- vars (定义变量，若已存在则会覆盖)

### Plugins
开发一个插件需要指定包名为com.github.meanstrong.aframework.plugins.action，类必须继承自ActionBase，复写其中的run方法，如下所示：   
```java
package com.github.meanstrong.aframework.plugins.action;
public class NewAction extends ActionBase {
    public NewAction(Task task) {
		super(task);
	}

    @Override
	public Map<String, Object> run(Map<String, Object> task_vars) {
        Object args = this.task.args; //args是task中Action对应的参数，可以以map形式取出各值。
        // to do something
    }
}
```
将写好的插件生成jar后放置于requires目录下，运行AFramework时将自动加载。

## Release
- [AFramework-0.0.1.jar](https://github.com/meanstrong/AFramework-java/releases/download/v0.0.1/AFramework-0.0.1.jar)

## Author
- <a href="mailto:pmq2008@gmail.com">Rocky Peng</a>

> AFramework Python版本正在开发中，预计近期也会发布出来。
